<?php
/**
 * Created by PhpStorm.
 * User: nkhiem
 * Date: 9/21/16
 * Time: 3:26 PM
 */

define('DIRECTORY', '/Users/nkhiem/Work/www/downloadimages'); //đổi thư mục để lưu trữ
define('HOST', 'http://tuoitre24h.net/wp-content/'); // đổi tên host theo danh sách link
define('HOST_LENGTH', $lenHost = strlen(HOST));

$f = fopen("dslink.txt", "r");

// Read line by line until end of file
while (!feof($f)) {
    $url = fgets($f);
    $url = trim($url);
    $path = checkAndCreateFile($url);
    downloadFile($url, $path);
    echo($url . "<br />" . $path. "\r\n");
}
fclose($f);


function checkAndCreateFile($url)
{
    $dir = DIRECTORY . substr($url, HOST_LENGTH - 1);
    $parts = explode('/', $dir);
    $file = array_pop($parts);
    $file = trim($file);
    $dir = '';
    foreach ($parts as $part)
        if (!is_dir($dir .= "/$part")) mkdir($dir, 0777, true);

    return "$dir/$file";
}

function downloadFile($url, $path)
{
    $ch = curl_init($url);
    $fp = fopen($path, 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
}
